var junction = require('junction')
  , Message = junction.elements.Message
  , m = 'bot@bo.t'
  , p = 'b00tcontrasena'
  , permite = ['tu@huma.no','su@humano.no']
  , ytdl = require('ytdl-core')
  , ffmpeg = require('fluent-ffmpeg')
  , fs = require('fs')
  , shortid = require('shortid')

function slugify(text) {
  const a = 'àáäâèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;'
  const b = 'aaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------'
  const p = new RegExp(a.split('').join('|'), 'g')

  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(p, c =>
        b.charAt(a.indexOf(c)))     // Replace special chars
    .replace(/&/g, '-and-')         // Replace & with 'and'
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '')             // Trim - from end of text
}

var opciones = {
  type: 'client',
  jid: m,
  password: p
};

var xmpp = junction.create();

xmpp.connect(opciones).on('online', function() {
  console.log('Conectado como: ' + this.jid);
  this.send(new junction.elements.Presence());
});

xmpp.use(junction.presence(function(handler) {
  handler.on('available', function(stanza) {
    console.log(stanza.from + ' está disponible');
  });
  handler.on('unavailable', function(stanza) {
    console.log(stanza.from + ' no está disponible');
  });
}));

xmpp.use(junction.messageParser());

xmpp.use(junction.message(function(handler) {
  handler.on('chat', function(stanza) {
    //console.log(stanza);
   // var quien = stanza.from;
   // var eljid = quien.split('/');
   // var elusu = eljid.split('@');
   // console.log('el JID: '+ eljid[0]);
   // console.log('el usuario: '+ elusu[0]);
   // si el mensaje que recibe empieza por Hola y es un usuario permitido
    if ( stanza.body.startsWith('Hola') && permite.includes(stanza.from.split('/')[0]) ) {
	console.log(stanza.body);

    	var msg = new Message(stanza.from);
    	msg.c('body', {}).t('Hola ' + stanza.from.split('@')[0] + '! soy un bot musical que puede convertir tus vídeos preferidos de youtube en música mp3, funciono así, mándeme este mensaje, un punto, una y griega, un espacio y la url del vídeo:\n\n' + '.Y https://www.youtube.com/watch?v=xxxxx');
    	//msg.c('body', {}).t('Hello ' + stanza.from + '!\n\n' + 'You said: ' + stanza.body);
    	stanza.connection.send(msg);
    }
    
    if ( stanza.body.startsWith('.Y') && permite.includes(stanza.from.split('/')[0]) ) {
	console.log(stanza.body);
    
	var url = stanza.body.split(' ')[1];


	// si es una url válida se saca el id
	if ( ytdl.validateURL(url) ) {
		console.log('url valida');
		ytdl.getBasicInfo(url, function(err, info){
			var nombre = slugify(info.title)
			console.log(info.title);
			console.log(nombre);
  			var si = shortid.generate()


			ytdl(url)
  				.pipe(fs.createWriteStream('videos/'+ si))
  				.on('finish', function() { 

					ffmpeg({ source: 'videos/'+ si}).saveToFile('musica/'+ nombre+si +'.mp3').on('end', function(){
						var msg = new Message(stanza.from);
        					msg.c('body', {}).t('Buen gusto. Puedes bajarte la canción en → http://bot.net/'+ nombre+si +'.mp3');
        					stanza.connection.send(msg);
					});

			});

		});

	}

    }

  });
}));

xmpp.use(junction.serviceUnavailable());
xmpp.use(junction.errorHandler());

